`timescale 1ns / 1ps

//Read 64bit audio file (raw format, only audio samples - no header)
module audio_file_read(
    input wire clk,
    output bit [63:0] audio_out
    );
    
parameter PATH = "./noisy_signal_float.raw";
parameter NR_OF_SAMPLES = 4435;

reg [63:0] audio_file [0:NR_OF_SAMPLES];

int counter = 0;

initial begin
    int fd;
    int n_Temp;
    
    fd = $fopen(PATH, "rb");
    if (fd) 
        $display("OK");
    else begin 
        $display("NOK"); 
        $finish; 
    end
    
    n_Temp = $fread(audio_file, fd);
    
    while (!$feof(fd))
        n_Temp = $fscanf(fd, "%b", audio_file);

    $fclose(fd);
end

always@(posedge clk) begin
    audio_out <= {
    audio_file[counter][7:0],
    audio_file[counter][15:8],
    audio_file[counter][23:16],
    audio_file[counter][31:24],
    audio_file[counter][39:32],
    audio_file[counter][47:40],
    audio_file[counter][55:48],
    audio_file[counter][63:56]
    };
        
    if(counter >= NR_OF_SAMPLES)
        counter = 0;
    else
        counter++;
end

endmodule