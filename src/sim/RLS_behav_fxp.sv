`timescale 1ns / 1ps

module RLS_behav_fxp(
    input wire clk,
    input bit [63:0] noise,
    input bit [63:0] noisy_signal,
    output bit [63:0] recovered_signal
);

//--- Filter Parameters ---
parameter SIZE = 8;
parameter FXP_SHIFT = 28;
parameter FXP_SCALE = 2**FXP_SHIFT;

//--- FXP functions ---
function bit signed [63:0] IntToFXP(input bit signed [63:0] x);
    return (x * FXP_SCALE); // <<
endfunction 

function bit signed [63:0] FXPToInt(input bit signed [63:0] x);
    return (x / FXP_SCALE); // >>
endfunction 

function bit signed [63:0] MUL(input bit signed [63:0] x, input bit signed [63:0] y); 
    bit signed [127:0] mulResult;
    mulResult = (x*y);
    return (mulResult >> FXP_SHIFT);
endfunction 

function bit signed [63:0] DIV(input bit signed [63:0] x, input bit signed [63:0] y);
    bit signed [127:0] intermVal;
    intermVal = x << FXP_SHIFT;
    return (intermVal / y);
endfunction 

//--- Declarations ---
bit signed [63:0] pi[SIZE];
bit signed [63:0] u[SIZE];
bit signed [63:0] k[SIZE];
bit signed [63:0] w[SIZE];
bit signed [63:0] P[SIZE][SIZE];
bit signed [63:0] P_pr[SIZE][SIZE];
bit signed [63:0] gamma = 0;
bit signed [63:0] lambda = 1 * FXP_SCALE;
bit signed [63:0] delta = 0.2 * FXP_SCALE;
bit signed [63:0] alfa;
bit signed [63:0] d;
    
//--- Initialize ---
int counter = 0;        
int i,j;

initial 
begin
    for (i=0; i<SIZE; i++)begin
        pi[i] = 0;
        u[i] = 0;
        k[i] = 0;
        w[i] = 0;
    end
    
    //initialize P
    for(i=0; i<SIZE; i++)
        for( j=0; j<SIZE; j++ )
            if (i == j)
                P[i][j] = delta;
            else
                P[i][j] = 0;
end

//--- Algorithm ---
always@(posedge clk) begin
    u[1:SIZE-1] = u[0:SIZE-2];
    u[0] = noise;
    d = noisy_signal;
    
    //pi = u'*P;
    for(i=0; i<SIZE; i++)begin
        pi[i] = 0;
        for( j=0; j<SIZE; j++)
            pi[i] += MUL(u[j],P[j][i]);
    end
    
    //gamma = lambda + pi*u;
    gamma = 0;
    for(i=0; i<SIZE; i++)
        gamma += MUL(pi[i], u[i]);
        
    gamma += lambda;
    
    //k = (pi')/gamma;
    for(i=0; i<SIZE; i++)
        k[i] = DIV(pi[i], gamma);
        
    //alfa(i) = noisy_signal(i) - w'*u;
    alfa = 0;
    for(i=0; i<SIZE; i++)
        alfa += MUL(w[i], u[i]);
    
    alfa = d - alfa;
    
    //w = w+k.*alfa(i);
    for(i=0; i<SIZE; i++)
        w[i] = w[i] + MUL(k[i], alfa);
    
    //P = (P-k*pi)/lambda;
    for(i=0; i<SIZE; i++)
        for( j=0; j<SIZE; j++)
            P_pr[i][j] = MUL(k[i], pi[j]);
            
    for(i=0; i<SIZE; i++)
        for( j=0; j<SIZE; j++)
            P[i][j] = DIV((P[i][j] - MUL(k[i], pi[j])), lambda);
            
    recovered_signal = alfa;
end
endmodule
