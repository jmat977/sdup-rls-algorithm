`timescale 1ns / 1ps

module tb(
    output bit [31:0] recovered_signal
); 

wire signed [15:0] noise_out;
wire signed [15:0] noisy_signal_out;

wire [15:0] noise_out_V2;
wire [15:0] noisy_signal_out_V2;

assign noise_out_V2 = noise_out + 32768;//* 65536;
assign noisy_signal_out_V2 = noisy_signal_out + 32768;// * 65536;

parameter CLOCK_PERIOD = 22676; // in ns
parameter NR_OF_SAMPLES = 4435;

reg clk = 1'b0;
always #(CLOCK_PERIOD/2) clk = ~clk;

//--- Filter Parameters ---

parameter SIZE = 8;
parameter FXP_SHIFT = 28;
parameter FXP_SCALE = 2**FXP_SHIFT; //FXP - 2^16

//bit [63:0] mulResult;
bit [63:0] alfa;
bit [63:0] beta;
bit signed [63:0] beka;
bit [63:0] d;
bit [63:0] g;
    
//--- Initialize ---
int counter = 0;        
int i,j;

function bit [63:0] IntToFXP(input bit [63:0] x);
    return (x * FXP_SCALE); // <<
endfunction 

function bit [63:0] FXPToInt(input bit [63:0] x);
    return (x / FXP_SCALE); // >>
endfunction 

//function bit [31:0] MUL(input bit [63:0] x, input bit [63:0] y); 
//    return ((x*y) >> FXP_SHIFT);
//endfunction 

//OR

function bit [63:0] MUL(input bit [63:0] x, input bit [63:0] y); 
    bit [127:0] mulResult;
    mulResult = (x*y);
    $display($sformatf("FXP: mulResult = %d:", mulResult));
    return (mulResult >> FXP_SHIFT);
endfunction 

function bit [63:0] DIV(input bit [63:0] x, input bit [63:0] y);
    bit [127:0] intermVal;
    intermVal = x << FXP_SHIFT;
    
    $display($sformatf("FXP: intermVal = %d:", intermVal));
    return (intermVal / y);
endfunction 

real resultFXP,mult;
real a,b,c;


initial 
begin
    a=0.245*FXP_SCALE;
    b=0.465*FXP_SCALE;
//    alfa = IntToFXP(64'd655);
//    beta = IntToFXP(64'd96);
    d = MUL($rtoi(a),$rtoi(b));
    g = DIV($rtoi(a),$rtoi(b));
    alfa = $rtoi(a);
    beta = $rtoi(b);
    beka = alfa - beta;
    
    $display($sformatf("FXP: alfa * beta = %d:", d));
    $display($sformatf("INT: alfa * beta = %d:", FXPToInt(d)));
    mult = d;
    mult = mult/FXP_SCALE;
    $display($sformatf("REAL: alfa * beta = %f:", mult));
    
    $display($sformatf("FXP: alfa / beta = %d:", g));
    $display($sformatf("INT: alfa / beta = %f:", FXPToInt(g)));
    #20;
    
    resultFXP = beka;
    resultFXP = resultFXP / FXP_SCALE;
    $display($sformatf("REAL: a - b = %f:", resultFXP));
    
    $finish;
end
    
endmodule
