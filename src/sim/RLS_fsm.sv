`timescale 1ns / 1ps

module RLS_fsm(
    input wire clk,
    input wire rst,
    input logic start,
    output logic ready,
    input bit new_sample,
    input bit [63:0] noise,
    input bit [63:0] noisy_signal,
    output bit [63:0] recovered_signal
    );
//--- Filter Parameters ---
parameter SIZE = 8;
parameter FXP_SHIFT = 28;
parameter FXP_SCALE = 2**FXP_SHIFT;

//--- FXP functions ---
function bit signed [63:0] MUL(input bit signed [63:0] x, input bit signed [63:0] y); 
    bit signed [127:0] mulResult;
    mulResult = (x*y);
    return (mulResult >> FXP_SHIFT);
endfunction 

function bit signed [63:0] DIV(input bit signed [63:0] x, input bit signed [63:0] y);
    bit signed [127:0] intermVal;
    intermVal = x << FXP_SHIFT;
    return (intermVal / y);
endfunction 

//Instantiate multiplier
logic mul_start;
logic mul_ready;
logic mul_finish;
logic[8:0] mul_count;
logic[63:0] mul_input0;
logic[63:0] mul_input1;
logic[63:0] mul_output0;

array_mult_8x8 array_mult_inst(
    .clk(clk),
    .start(mul_start),
    .ready(mul_ready),
    .count(mul_count),
    .finished(mul_finish),
    .input_array_0(mul_input0),
    .input_array_1(mul_input1),
    .output_array(mul_output0)
);
logic [3:0] i=0, j=0;
logic [2:0] ii, jj, kk;
assign ii = mul_count[8:6];
assign jj = mul_count[5:3];
assign kk = mul_count[2:0];

//--- Declarations ---
bit signed [63:0] pi[SIZE];
bit signed [63:0] u[SIZE];
bit signed [63:0] k[SIZE];
bit signed [63:0] w[SIZE];
bit signed [63:0] P[SIZE][SIZE];
bit signed [63:0] P_pr[SIZE][SIZE];
bit signed [63:0] gamma = 0;
bit signed [63:0] lambda = 1 * FXP_SCALE;
bit signed [63:0] delta = 0.2 * FXP_SCALE;
bit signed [63:0] alfa;
bit signed [63:0] d;

//FSM
enum {S0=0, S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11, S12} state;

always_ff @(posedge clk) begin: fsm
case(state)
S0: begin
    ready <= 1'b0;
    if (start == 1'b0) begin
        state <= S0;
    end else begin
        state <= S1;
    end 
end
S1: begin
    pi[i] <= 0;
    u[i] <= 0;
    k[i] <= 0;
    w[i] <= 0;
    
    if(i == j)
        P[i][j] <= delta;
    else
        P[i][j] <= 0;
       
    i <= i + 1;
    
    state <= S2;
end
S2: begin
    j <= j + 1;
    state <= S1;
    if(i == SIZE)begin
        i <= 0;
        if(j == SIZE-1)begin
            j <= 0;
            state <= S3;
        end 
    end
end
S3: begin
    ready <= 1'b0;
    if(new_sample)begin
        u <= {noise, u[0:SIZE-2]};
        d <= noisy_signal;
        state <= S4;
    end else
        state <= S3;
end
S4: begin //pi = u'*P;
    if ( mul_ready == 1'b1 ) begin
        if (jj == 0)begin
            pi[kk] <= mul_output0;
            state <= S4;
        end
        if( mul_finish == 1'b1)
            state <= S5;
    end else begin
        state <= S4; // cont. waiting
    end
    end
S5: begin //gamma = lambda + pi*u;
    if ( mul_ready == 1'b1 ) begin
        if (jj == 0 && kk == 0)begin
            gamma <= mul_output0 + lambda;
            state <= S5;
        end
        if( mul_finish == 1'b1)
            state <= S6;
    end else begin
        state <= S5; // cont. waiting
    end
    end
S6: begin  //k = (pi')/gamma;
    k[i] <= DIV(pi[i], gamma);
    if(i < SIZE-1)begin
        state <= S6;
        i <= i + 1; 
    end else begin
        state <= S7;
        i <= 0;   
    end
    end
S7: begin //alfa(i) = noisy_signal(i) - w'*u;
    if ( mul_ready == 1'b1 ) begin
        if (jj == 0 && kk == 0)begin
            alfa <= d - mul_output0;
            state <= S7;
        end
        if( mul_finish == 1'b1)
            state <= S8;
    end else begin
        state <= S7; // cont. waiting
    end
    end
S8: begin  //w = w+k.*alfa(i);
    w[i] <= w[i] + MUL(k[i], alfa);
    if(i < SIZE-1)begin
        state <= S9;
        i <= 0;    
    end else begin
        state <= S8;
        i <= i + 1;
    end
    end
S9: begin  //P_pr = k*pi;
    if ( mul_ready == 1'b1 ) begin
        if (jj == 0 && kk == 0)begin
            P_pr[jj][kk] <= mul_output0;
            state <= S9;
        end
        if( mul_finish == 1'b1)
            state <= S10;
    end else begin
        state <= S9; // cont. waiting
    end
end
S10: begin
    P[i][j] <= DIV(P[i][j] - P_pr[i][j], lambda);
    i <= i + 1;
    state <= S11;
end
S11: begin
    j <= j + 1;
    state <= S10;
    if(i == SIZE)begin
        i <= 0;
        if(j == SIZE-1)begin
            j <= 0;
            state <= S12;
        end 
    end
end
S12: begin
    ready <= 1'b1;
    recovered_signal <= alfa;
    state <= S3;
end
endcase
end: fsm

always_comb begin
case(state)
S4: begin //pi = u'*P;
    mul_start <= ~mul_ready;
    
    if(jj == 0)
    mul_input0 <= u[kk];
    else
    mul_input0 <= 0;
    
    mul_input1 <= P[jj][kk];
end
S5: begin //gamma = lambda + pi*u;
    mul_start <= ~mul_ready;
    
    if(jj == 0)
    mul_input0 <= pi[kk];
    else
    mul_input0 <= 0;
    
    if(kk == 0)
    mul_input1 <= u[jj];
    else
    mul_input1 <= 0;
    
end
S7: begin //alfa(i) = noisy_signal(i) - w'*u;
    mul_start <= ~mul_ready;
    
    if(jj == 0)
    mul_input0 <= w[kk];
    else
    mul_input0 <= 0;
    
    if(kk == 0)
    mul_input1 <= u[jj];
    else
    mul_input1 <= 0;
    
end
S9: begin //P_pr = k*pi; 
    mul_start <= ~mul_ready;
    
    if(kk == 0)
    mul_input0 <= k[jj];
    else
    mul_input0 <= 0;
    
    if(jj == 0)
    mul_input1 <= pi[kk];
    else
    mul_input1 <= 0;
    
end
default: begin
    mul_start <= 1'b0;
end
endcase
end
endmodule
