`timescale 1ns / 1ps
module RLS_behav(
    input wire clk,
    input bit [63:0] noisy_signal,
    input bit [63:0] noise,
    output bit [63:0] alfa_out
);

real noisy_signal_sample;
real noise_sample;

//--- Filter Parameters ---

parameter SIZE = 5;

real pi[SIZE];
real u[SIZE];
real k[SIZE];
real w[SIZE];
real P[SIZE][SIZE];
real P_pr[SIZE][SIZE];
real gamma = 0;
real lambda = 1;
real delta = 0.2;
real alfa;
real d;
    
//--- Initialize ---
int counter = 0;        
int i,j;

initial 
begin
    for (i=0; i<SIZE; i++)begin
        pi[i] = 0;
        u[i] = 0;
        k[i] = 0;
        w[i] = 0;
    end
    
    //initialize P
    for(i=0; i<SIZE; i++)
        for( j=0; j<SIZE; j++ )
            if (i == j)
                P[i][j] = delta;
            else
                P[i][j] = 0;
end


always@(posedge clk) begin
    noisy_signal_sample = $bitstoreal(noisy_signal);
    noise_sample = $bitstoreal(noise);

    u[1:SIZE-1] = u[0:SIZE-2];
    u[0] = noise_sample;
    d = noisy_signal_sample;

    //pi = u'*P;
    for(i=0; i<SIZE; i++)begin
        pi[i] = 0;
        for( j=0; j<SIZE; j++)
            pi[i] += u[j] * P[j][i];
    end
    
    //gamma = lambda + pi*u;
    gamma = 0;
    for(i=0; i<SIZE; i++)
        gamma += pi[i] * u[i];
    
    gamma += lambda;
    
    //k = (pi')/gamma;
    for(i=0; i<SIZE; i++)
        k[i] = pi[i] / gamma;
    
    //alfa(i) = noisy_signal(i) - w'*u;
    alfa = 0;
    for(i=0; i<SIZE; i++)
        alfa += w[i] * u[i];
           
    alfa = d - alfa;
    
    //w = w+k.*alfa(i);
    for(i=0; i<SIZE; i++)
        w[i] = w[i] + k[i] * alfa;
    
    //P = (P-k*pi)/lambda;
    for(i=0; i<SIZE; i++)
        for( j=0; j<SIZE; j++)
            P_pr[i][j] = k[i] * pi[j];
            
    for(i=0; i<SIZE; i++)
        for( j=0; j<SIZE; j++)
            P[i][j] = (P[i][j] - P_pr[i][j]) / lambda;
    
    alfa_out = $realtobits(alfa);
end

endmodule
