`timescale 1ns / 1ps

module array_mult_8x8(
    input logic clk,
    input logic start,
    input logic [63:0] input_array_0,
    input logic [63:0] input_array_1,
    output logic [63:0] output_array,
    output logic [8:0] count,
    output logic ready,
    output logic finished
);
//--- FXP functions ---
parameter FXP_SHIFT = 28;
parameter FXP_SCALE = 2**FXP_SHIFT;

function bit signed [63:0] MUL(input bit signed [63:0] x, input bit signed [63:0] y); 
    bit signed [127:0] mulResult;
    mulResult = (x*y);
    return (mulResult >> FXP_SHIFT);
endfunction 

//FSMD
enum {IDLE=0, S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11, S12, S13, S14, S15, S16, S17} state;

logic [63:0] in1 = 64'h0, in2 = 64'h0, out1 = 64'h0;
logic [127:0] a = 128'h0, b = 128'h0, P = 128'h0, Sum = 128'h0;

logic [2:0] ii, jj, kk;
assign ii = count[8:6];
assign jj = count[5:3];
assign kk = count[2:0];

logic [63:0] array_A [0:63], array_B [0:63],  array_C [0:63];

always @(posedge clk) begin: fsmd
case(state)
IDLE: begin
    ready    <= 1'b0;
    finished <= 1'b0;
    count   <= 9'h0;
    output_array <= 64'h0;
    if (start == 1'b0) begin
        state <= IDLE;
    end else begin
        state <= S1;
    end
end

S1: begin
    in1 <= input_array_0;
    in2 <= input_array_1;
    state <= S2;
end

S2: begin
    array_A[{jj,kk}] <= in1;
    array_B[{jj,kk}] <= in2;
    state <= S3;
end

S3: begin
    if (count == 63)begin
        state <= S5;
    end
    else begin
        state <= S4;
    end
end

S4: begin
    count <= count + 1;
    state <= S1;
end

S5: begin
    count <= 0;
    state <= S6;
end

S6: begin
    a <= array_A[{ii, kk}];
    b <= array_B[{kk, jj}];
    state <= S7;
end

S7: begin
    P <= MUL(a,b);
    if (kk == 0)
        state <= S8;
    else
        state <= S9;
end

S8: begin
    Sum <= 0;
    state <= S9;
end

S9: begin
    Sum <= Sum + P;
    if (kk == 7)
        state <= S10;
    else 
        state <= S11;
end

S10: begin
    array_C[{ii,jj}] <= Sum;
    state <= S11;
end

S11: begin
    if (count != 511)
        state <= S12;
    else 
        state <= S13;
end

S12: begin
    count <= count + 1;
    state <= S6;
end

S13: begin
    ready <= 1'b1;
    count <= 9'h0;
    state <= S14;
end

S14: begin
    out1 <= array_C[{jj,kk}];
    state <= S15;
end

S15: begin
    output_array <= out1;
    state <= S16;
end

S16: begin
    if (count == 63)begin
        state <= IDLE;
        finished <= 1'b1;
        end
    else
        state <= S17;
end

S17: begin
    count <= count + 1;
    state <= S14;
end
endcase

end: fsmd

endmodule
