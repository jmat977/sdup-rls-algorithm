`timescale 1ns / 1ps

module RLS_fsm_tb(
    output bit [63:0] recovered_signal
);

logic start, ready, new_sample;

parameter NR_OF_SAMPLES = 4435;
int counter = 0;  

parameter FXP_SHIFT = 28;
parameter FXP_SCALE = 2**FXP_SHIFT;

wire signed [15:0] noise_out;
wire signed [15:0] noisy_signal_out;

wire [63:0] noise_out_FXP;
wire [63:0] noisy_signal_out_FXP;

//Change signal amplitude range to 0 - 2 (in FXP format)
assign noise_out_FXP = (noise_out + 32768)*(2**(FXP_SHIFT-15)); 
assign noisy_signal_out_FXP = (noisy_signal_out + 32768)*(2**(FXP_SHIFT-15));

//--- Clock generation ---
parameter CLOCK_PERIOD = 22676*8; // in ns
reg clk_audio = 1'b0;
reg clk100MHz = 1'b0;

initial
begin
#10
start = 1;
new_sample = 1;
#200
new_sample = 0;
#(CLOCK_PERIOD/2);
forever
    #(CLOCK_PERIOD/2) clk_audio = ~clk_audio;
end

//always #(CLOCK_PERIOD/2) clk_audio = ~clk_audio;
always #(5) clk100MHz = ~clk100MHz;

//--- Module instances ---
audio_file_read #(
    .PATH("./noise_s16PCM.raw"),
    .NR_OF_SAMPLES(NR_OF_SAMPLES),
    .BITS(16)
    ) read_noise (
    .clk(clk_audio),
    .audio_out(noise_out)
    );  
     
audio_file_read #(
    .PATH("./noisy_signal_s16PCM.raw"),
    .NR_OF_SAMPLES(NR_OF_SAMPLES),
    .BITS(16)
    ) read_noisy_signal (
    .clk(clk_audio),
    .audio_out(noisy_signal_out)
    );         

RLS_fsm inst_RLS_fsm(
    .clk(clk100MHz),
    .rst(),
    .start(start),
    .ready(ready),
    .new_sample(new_sample),
    .noise(noise_out_FXP),
    .noisy_signal(noisy_signal_out_FXP),
    .recovered_signal(recovered_signal)
    );

//--- Finish at the end of file ---
always@(posedge clk_audio) begin
    counter++;
    
    new_sample = 1;
    #100
    new_sample = 0;
    if (counter == NR_OF_SAMPLES)
    $finish;
end

endmodule
