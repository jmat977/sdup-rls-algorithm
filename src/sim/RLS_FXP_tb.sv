`timescale 1ns / 1ps

module RLS_FXP_tb(
    output bit [63:0] recovered_signal
);

parameter NR_OF_SAMPLES = 4435;
int counter = 0;  

parameter FXP_SHIFT = 28;
parameter FXP_SCALE = 2**FXP_SHIFT;

wire signed [15:0] noise_out;
wire signed [15:0] noisy_signal_out;

wire [63:0] noise_out_FXP;
wire [63:0] noisy_signal_out_FXP;

//Change signal amplitude range to 0 - 2 (in FXP format)
assign noise_out_FXP = (noise_out + 32768)*(2**(FXP_SHIFT-15)); 
assign noisy_signal_out_FXP = (noisy_signal_out + 32768)*(2**(FXP_SHIFT-15));

//--- Clock generation ---
parameter CLOCK_PERIOD = 22676; // in ns
reg clk = 1'b0;

always #(CLOCK_PERIOD/2) clk = ~clk;

//--- Module instances ---
audio_file_read #(
    .PATH("./noise_s16PCM.raw"),
    .NR_OF_SAMPLES(NR_OF_SAMPLES),
    .BITS(16)
    ) read_noise (
    .clk(clk),
    .audio_out(noise_out)
    );  
     
audio_file_read #(
    .PATH("./noisy_signal_s16PCM.raw"),
    .NR_OF_SAMPLES(NR_OF_SAMPLES),
    .BITS(16)
    ) read_noisy_signal (
    .clk(clk),
    .audio_out(noisy_signal_out)
    );         

RLS_behav_fxp inst_RLS_behav(
    .clk(clk),
    .noisy_signal(noisy_signal_out_FXP),
    .noise(noise_out_FXP),
    .recovered_signal(recovered_signal)
);

//--- Finish at the end of file ---
always@(posedge clk) begin
    counter++;  
    if (counter == NR_OF_SAMPLES)
    $finish;
end

endmodule
