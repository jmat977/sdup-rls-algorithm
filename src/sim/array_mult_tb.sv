`timescale 1ns / 1ps

module array_mult_tb(
    logic [63:0] output_array
);

parameter CLOCK_PERIOD = 10; // in ns
parameter NR_OF_SAMPLES = 4435;

logic start, done, finished;
logic [8:0] count;
logic [63:0] A[64], B[64], C[64];

initial begin
A = '{9,6,3,2,1,4,5,6 ,5,4,5,6,6,5,4,5 ,1,2,3,4,5,6,7,8 ,0,0,0,0,0,0,0,0 ,0,0,0,0,0,0,0,0 ,0,0,0,0,0,0,0,0 ,0,0,0,0,0,0,0,0 ,0,0,0,0,0,0,0,0};
B = '{5,2,0,0,0,0,0,0 ,6,8,0,0,0,0,0,0 ,5,6,0,0,0,0,0,0 ,4,5,0,0,0,0,0,0 ,5,6,0,0,0,0,0,0 ,7,7,0,0,0,0,0,0 ,9,1,0,0,0,0,0,0 ,3,3,0,0,0,0,0,0};
end

reg clk = 1'b0;
always #(CLOCK_PERIOD/2) clk = ~clk;

array_mult_8x8 array_mult_inst(
    .clk(clk),
    .start(start),
    .input_array_0(A[count]),
    .input_array_1(B[count]),
    .count(count),
    .output_array(output_array),
    .ready(done),
    .finished(finished)
);

initial begin
    #5
    start = 1;
    
end

    always @(posedge clk)
    begin
        if(done)
            C[count] <= output_array;
    end
endmodule
