`timescale 1ns / 1ps

module audio_read_tb(
    output bit [63:0] noise_out,
    output bit [63:0] noisy_signal_out
    );

parameter CLOCK_PERIOD = 22676; // in ns
parameter NR_OF_SAMPLES = 4435;

reg clk = 1'b0;

always #(CLOCK_PERIOD/2) clk = ~clk;

int counter = 0;

audio_file_read #(
    .PATH("./noise_float.raw"),
    .NR_OF_SAMPLES(NR_OF_SAMPLES)
    ) read_noise (
    .clk(clk),
    .audio_out(noise_out)
    );  
     
audio_file_read #(
    .PATH("./noisy_signal_float.raw"),
    .NR_OF_SAMPLES(NR_OF_SAMPLES)
    ) read_noisy_signal (
    .clk(clk),
    .audio_out(noisy_signal_out)
    );   

always@(posedge clk) begin
    counter++;  
    if (counter == NR_OF_SAMPLES)
    $finish;
end

endmodule