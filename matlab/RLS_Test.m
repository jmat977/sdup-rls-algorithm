%% RLS Adaptive Noise Canceller
clc;
close all;
clear all;

fs = 44100; % Sampling frequency (samples per second) 
dt = 1/fs; % seconds per sample 
StopTime = 0.5; % seconds 
t = (0:dt:StopTime)'; % seconds 
F = 60; % Sine wave frequency (hertz) 

signal_clean = sin(2*pi*F*t);

noise = 0.3*randn(size(signal_clean, 1),1);

noisy_signal = signal_clean + noise;

t = t';

%% Plot references

figure, plot(t, noise),
xlabel('time [s]');
ylabel('amplitude [V]');
title('Reference Noise');
axis tight

figure, plot(t, noisy_signal),
xlabel('time [s]');
ylabel('amplitude [V]');
title('Noisy Signal');
axis tight

%% RLS ANC

%RLS Parameters
M = 7; %filter length
lambda = 1; %forgeting factor
%if lambda is close to 1 then less fluctuations in the filter coefficients
%lambda = 1 -> growing window RLS Algorithim
lambdainv = 1/lambda;
delta = 5; %small positive constant
deltainv = 1/delta;

%Filter init
N = length(noisy_signal); %number of samples in the input x
w = zeros(M,1); %init filter coeffs
P = deltainv*eye(M); %inverse correlation matrix
e = zeros(N,1); %error signal

noise = noise(:);
noisy_signal = noisy_signal(:);
m = 0; %number of iterations performed

% ANC using RLS Algorithm
for i = M:N
    % obtain reference input
    y = noise(i:-1:i-M+1);
    % error signal eqn. - estimated output signal
    e(i) = noisy_signal(i)-w'*y;
    % filter gain vector updated
    k = (P*y)/(lambda+y'*P*y);
    % Inverse correlation matrix updated
    P = (P - k*y'*P)*lambdainv;
    % filter coeffs updated using RLS
    w = w + k*e(i);
    m = m+1; % iteration counter
    w1(m,:) = w(:,1); % each column of matrix w1 represents the history of each filter coefficients
end

e = normalize(e, 'range', [-1 1]);

%% Plot output

fprintf('Number of iterations performed: %d\n', m);

figure,
subplot(3,1,1), plot(signal_clean); title('Original signal'); axis tight,
subplot(3,1,2), plot(noisy_signal); title('Noisy signal'); axis tight,
subplot(3,1,3), plot(e); title('Estimated(recovered) signal (e)'); axis tight,

% Adaptation of filter coefficients over time
figure, 
plot(w1)
title('Adaptation of filter coefficients over time')
axis tight