# SDUP - RLS Algorithm


<p align="center">
 <b>Created by Rafał Ziobrowski & Jakub Maternowski for SDUP class</b>
</p>
<p align="center">
 AGH University of Science and Technology in Cracow
</p>
 <p align="center">
Microelectronics in Industry and Medicine 2021
</p>

## Project specifiaction

1. Project implemented on Xilinx Zybo Z7-10 board
2. Written in Vivado 2018.3


## Creating Vivado project
1. Clone this repository into your working directory:
```
git clone https://gitlab.com/jmat977/sdup-rls-algorithm.git
```
2. Open Vivado 2018.3
3. Open Tcl Console (Window -> Tcl Console or Ctrl+Shift+T)
4. Navigate to directory "vivado" inside your working directory for example:
```
cd C:/Your/path/to/working/directory/vivado
```
5. Insert and run command:
```
source ./RLS_Algorithm.tcl
```

6. Running the simulation



## Gallery


## Video



